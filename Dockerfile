FROM registry.gitlab.com/making.ventures/images/node-base

# RUN apt-get update
# RUN apt-get dist-upgrade -y
# RUN apt-get install -y --no-install-recommends ffmpeg
# RUN apt-get clean

RUN set -x \
  && apt-get update \
  && apt-get dist-upgrade -y \
  && apt-get install -y --no-install-recommends ffmpeg \ 
  && apt-get clean
